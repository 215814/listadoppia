#include <iostream>
#include <fstream>
#include "liste.h"

using namespace std;


lista creaAgenda(){
	
	lista agenda=new lista;
	ifstream fin("agenda.txt");
	while(!fin.eof()){
		elem *e=new elem();

		char c;
		fin>>c; //leggo fino a spazio o a capo
		e.inf.data=c;
		fin>>c;
		e.inf.ora_i=c;
		fin>>c;
		e.inf.ora_f=c;
	
		fin.get(c); //leggo anche gli spazi e fine riga		
		e.inf.descr=c;
		agenda=insert_elem(agenda,e);
		
	}
	
	fin.close();
	
	return agenda;

}


int main(){
	int scelta;
	
	while(true){
		lista agenda=creaAgenda();
		cout<<"- - - - - AGENDA - - - - - \n";
		cout<<"1. crea appuntamento \n";
		cout<<"2. stampa agenda \n";
		cout<<"3. esci \n"<<endl;
		
		cin>>scelta;
		
		switch (scelta){
			case 1: {
				agenda=creaAgenda();
				break;
			}
			
			case 2:{
				break;
			}
			
			case 3:{
				return;
			}
			
			default: return;
		}
	}	
}